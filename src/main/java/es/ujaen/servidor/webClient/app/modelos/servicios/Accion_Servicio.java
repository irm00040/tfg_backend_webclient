package es.ujaen.servidor.webClient.app.modelos.servicios;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Miembro;
import es.ujaen.servidor.webClient.app.modelos.Miembro_Creador;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

public interface Accion_Servicio {
	
	public Mono<Accion> findById(String id);
	
	public Mono<Tablero> obtenerTablero(String idAccion);
	
	public Mono<Tarjeta> obtenerTarjeta(String idAccion);

	public Mono<Lista> obtenerLista(String idAccion);
	
	public Mono<Miembro> obtenerMiembros(String idAccion);
	
	public Mono<Miembro_Creador> obtenerMiembroCreador(String idAccion);
	

}
