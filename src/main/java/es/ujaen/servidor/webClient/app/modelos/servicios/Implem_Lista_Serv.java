package es.ujaen.servidor.webClient.app.modelos.servicios;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

@Service
public class Implem_Lista_Serv implements Lista_Servicio{

	@Autowired
	private WebClient client;
	

	@Override
	public Mono<Lista> findById(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		return client.get()
				.uri("/lists/{id}", params)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Lista.class);
	}


	@Override
	public Mono<Tablero> get_TableroLista(String idList) {
		Mono<Tablero> lista_tab = client.get()
				.uri("/lists/{idList}/board", Collections.singletonMap("idList", idList))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Tablero.class);
		return lista_tab;
	}

	@Override
	public Mono<List<Tarjeta>> get_TarjetasLista(String id) {
		Mono<List<Tarjeta>> listaTarjetas = client.get()
				.uri("/lists/{idList}/cards",Collections.singletonMap("idList", id))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Tarjeta.class)
				.collectList();
		return listaTarjetas;
	}



	@Override
	public Mono<List<Accion>> get_AccionesLista(String idList) {
		Mono<List<Accion>> listaAciones = client.get()
				.uri("/lists/{idList}/actions",Collections.singletonMap("idList", idList))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Accion.class)
				.collectList();
		return listaAciones;
	}





	

	
}