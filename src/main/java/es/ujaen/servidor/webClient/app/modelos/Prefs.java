package es.ujaen.servidor.webClient.app.modelos;

public class Prefs {

	private String permissionLevel;

	public String getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(String permissionLevel) {
		this.permissionLevel = permissionLevel;
	}
	
	
}
