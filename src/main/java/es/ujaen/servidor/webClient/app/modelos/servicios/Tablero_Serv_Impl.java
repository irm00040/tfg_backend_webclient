package es.ujaen.servidor.webClient.app.modelos.servicios;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Miembro;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

@Service
public class Tablero_Serv_Impl implements Tablero_Serv{

	@Autowired
	private WebClient client;


	@Override
	public Mono<Tablero> findById(String id) {
			
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		Mono<Tablero> tablero = client.get()
				.uri("/boards/{id}", params)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Tablero.class);
		return tablero;
	}
	
	public Mono<List<Miembro>> get_Miembros(String id) {
		Mono<List<Miembro>> listaMi = client.get()
				.uri("/boards/{id}/members",Collections.singletonMap("id", id))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Miembro.class)
				.collectList();

		return listaMi;
	}
	
	@Override
	public Mono<List<Tarjeta>> get_TarjetasTablero(String id) {
		Mono<List<Tarjeta>> listaTarjetas = client.get()
				.uri("/boards/{id}/cards",Collections.singletonMap("id", id))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Tarjeta.class)
				.collectList();
		return listaTarjetas;
	}
	
	@Override
	public Mono<List<Lista>> get_Listas(String idTab) {
		Mono<List<Lista>> listas = client.get()
				.uri("/boards/{idTab}/lists", Collections.singletonMap("idTab", idTab))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Lista.class)
				.collectList();
		return listas;
	}
	
	@Override
	public Mono<List<Accion>> get_AccionesTab(String idTab) {
		Mono<List<Accion>> listaAcciones = client.get()
				.uri("/boards/{idTab}/actions?limit=1000", Collections.singletonMap("idTab", idTab))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Accion.class)
				.collectList();
		return listaAcciones;
	}
	
	@Override
	public Mono<Tarjeta> get_DetallesTarjeta(String idTab, String idTarj) {
		Mono<Tarjeta> listaDetalle = client.get()
				.uri("/boards/{idTab}/cards/{idTarj}", idTab,idTarj)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Tarjeta.class);
		return listaDetalle;
	}
	
}
