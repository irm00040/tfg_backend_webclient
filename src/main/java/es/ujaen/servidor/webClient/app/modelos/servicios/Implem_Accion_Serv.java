package es.ujaen.servidor.webClient.app.modelos.servicios;


import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Miembro;
import es.ujaen.servidor.webClient.app.modelos.Miembro_Creador;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

@Service
public class Implem_Accion_Serv implements Accion_Servicio{

	@Autowired
	private WebClient client;
	
	@Override
	public Mono<Accion> findById(String id){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		return client.get()
				.uri("/actions/{id}", params)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Accion.class);
	}
	
	@Override
	public Mono<Tablero> obtenerTablero(String idAccion) {
		return client.get()
				.uri("/actions/{idAccion}/board", Collections.singletonMap("idAccion", idAccion))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Tablero.class);
	}

	@Override
	public Mono<Tarjeta> obtenerTarjeta(String idAccion) {
		return client.get()
				.uri("/actions/{idAccion}/cards", Collections.singletonMap("idAccion", idAccion))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Tarjeta.class);
	}


	@Override
	public Mono<Lista> obtenerLista(String idAccion) {
		return client.get()
				.uri("/actions/{idAccion}/lists", Collections.singletonMap("idAccion", idAccion))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Lista.class);
	}


	@Override
	public Mono<Miembro> obtenerMiembros(String idAccion) {
		return client.get()
				.uri("/actions/{idAccion}/members", Collections.singletonMap("idAccion", idAccion))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Miembro.class);
	}


	@Override
	public Mono<Miembro_Creador> obtenerMiembroCreador(String idAccion) {
		return client.get()
				.uri("/actions/{idAccion}/membersCreator", Collections.singletonMap("idAccion", idAccion))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Miembro_Creador.class);
	}

	
}