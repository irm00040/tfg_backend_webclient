package es.ujaen.servidor.webClient.app;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import es.ujaen.servidor.webClient.app.controlador.Controlador_Api;

import org.springframework.context.annotation.Bean;

@Configuration
//AQUI ES DONDE CONFIGURAMOS LAS RUTAS PARA MAPEAR

public class RouterConfig {
	
	@Bean
	public RouterFunction<ServerResponse> rutas(Controlador_Api handler){
		
		return RouterFunctions.route(RequestPredicates.GET("/servidor/tableros/{id}"),handler::get_Tablero)
				.andRoute(RequestPredicates.GET("/servidor/tableros/{id}/miembros"), handler::get_MiembrosTablero)
				.andRoute(RequestPredicates.GET("/servidor/tableros/{id}/tarjetas"), handler::get_TarjetasTablero)
				.andRoute(RequestPredicates.GET("/servidor/tableros/{idTab}/tarjetas/{idTarj}"), handler::get_DetalleTarjetas)
				.andRoute(RequestPredicates.GET("/servidor/tableros/{idTab}/listas"), handler::get_ListasTab)
				.andRoute(RequestPredicates.GET("/servidor/tableros/{idTab}/acciones"), handler::get_AccionesTab)
				
				//Tarjeta
				.andRoute(RequestPredicates.GET("/servidor/tarjetas/{id}/lista"), handler::get_ListaTarjeta)
				.andRoute(RequestPredicates.GET("/servidor/tarjetas/{id}"), handler::get_Tajerta)
				
				//Lista
				.andRoute(RequestPredicates.GET("/servidor/listas/{id}"), handler::get_Lista)
				.andRoute(RequestPredicates.GET("/servidor/listas/{id}/tablero"), handler::get_TableroLista)
				.andRoute(RequestPredicates.GET("/servidor/listas/{id}/tarjetas"), handler::get_TarjetasLista)
				
				//Miembro
				.andRoute(RequestPredicates.GET("/servidor/miembros/{id}"), handler::get_Miembro)
				.andRoute(RequestPredicates.GET("/servidor/miembros/{id}/acciones"), handler::get_AccionesMiembro)
				
				//Acción
				.andRoute(RequestPredicates.GET("/servidor/accion/{id}"), handler::get_Accion)//OK
				.andRoute(RequestPredicates.GET("/servidor/accion/{id}/tablero"), handler::listarTableroAcc)
				.andRoute(RequestPredicates.GET("/servidor/accion/{id}/tarjeta"), handler::listarTarjetaAcc)
				.andRoute(RequestPredicates.GET("/servidor/accion/{id}/lista"), handler::listarLitaAcc)
				.andRoute(RequestPredicates.GET("/servidor/accion/{id}/miembros"), handler::listarMiembrosAcc)
				.andRoute(RequestPredicates.GET("/servidor/accion/{id}/creador"), handler::listarMiemCreadorAcc)
				
				;
	}


}
