package es.ujaen.servidor.webClient.app.modelos;


public class Lista {
	
	private String id;
	private String name;
	private boolean closed;
	private Number pos;
	private String idBoard;
	private boolean subscribed;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	public Number getPos() {
		return pos;
	}
	public void setPos(Number pos) {
		this.pos = pos;
	}
	public String getIdBoard() {
		return idBoard;
	}
	public void setIdBoard(String idBoard) {
		this.idBoard = idBoard;
	}
	public boolean isSubscribed() {
		return subscribed;
	}
	public void setSubscribed(boolean subscribed) {
		this.subscribed = subscribed;
	}
	
	

}
