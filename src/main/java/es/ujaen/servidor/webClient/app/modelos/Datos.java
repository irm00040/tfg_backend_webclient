package es.ujaen.servidor.webClient.app.modelos;


public class Datos {
	
	private String text;
	private Lista_Data list;
	private Tarjeta_Data card;
	private Tablero_Data board;
	private Old old;
	
	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}

	public Tarjeta_Data getCard() {
		return card;
	}

	public void setCard(Tarjeta_Data card) {
		this.card = card;
	}


	public Tablero_Data getBoard() {
		return board;
	}


	public void setBoard(Tablero_Data board) {
		this.board = board;
	}


	public Old getOld() {
		return old;
	}

	public void setOld(Old old) {
		this.old = old;
	}


	public Lista_Data getList() {
		return list;
	}


	public void setList(Lista_Data list) {
		this.list = list;
	}
}
