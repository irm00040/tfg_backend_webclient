package es.ujaen.servidor.webClient.app.modelos.servicios;

import java.util.List;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Miembro;
import reactor.core.publisher.Mono;

public interface Miembro_Servicio {
		
	public Mono<Miembro> findById(String id);

	public Mono<List<Accion>> get_AccionesMiembro(String id);

}
