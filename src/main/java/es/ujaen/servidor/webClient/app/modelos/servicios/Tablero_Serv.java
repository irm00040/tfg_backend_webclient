package es.ujaen.servidor.webClient.app.modelos.servicios;


import java.util.List;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Miembro;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

public interface Tablero_Serv {
		
	public Mono<Tablero> findById(String id);
	
	public Mono<List<Miembro>> get_Miembros(String id);
	
	public Mono<List<Tarjeta>>get_TarjetasTablero(String id);
	
	public Mono<Tarjeta> get_DetallesTarjeta(String idTab, String idTarj);
	
	public Mono<List<Lista>> get_Listas(String idTab);
	
	public Mono<List<Accion>> get_AccionesTab(String idTab);

}
