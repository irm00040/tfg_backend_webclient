package es.ujaen.servidor.webClient.app.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import es.ujaen.servidor.webClient.app.modelos.servicios.Lista_Servicio;
import es.ujaen.servidor.webClient.app.modelos.servicios.Miembro_Servicio;
import es.ujaen.servidor.webClient.app.modelos.servicios.Tablero_Serv;
import es.ujaen.servidor.webClient.app.modelos.servicios.Tarjeta_Servicio;
import reactor.core.publisher.Mono;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.servicios.Accion_Servicio;

@Component
@CrossOrigin(origins= "http://localhost:4200")
public class Controlador_Api {


	@Autowired
	private Tablero_Serv tablero;
	
	@Autowired
	private Tarjeta_Servicio tarjeta;
	
	@Autowired
	private Lista_Servicio lista;
	
	@Autowired
	private Miembro_Servicio miembro;
	
	@Autowired
	private Accion_Servicio accion;
	
	
	public Mono<ServerResponse> get_Tablero(ServerRequest req){
		String id = req.pathVariable("id");
		return tablero.findById(id).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
				
	}
	
	public Mono<ServerResponse> get_MiembrosTablero(ServerRequest req){
		String id = req.pathVariable("id");
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(tablero.get_Miembros(id),Tablero.class);
	}
		
	public Mono<ServerResponse> get_TarjetasTablero(ServerRequest req){
		String id = req.pathVariable("id");
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(tablero.get_TarjetasTablero(id),Tablero.class);
	}
	
	public Mono<ServerResponse> get_ListasTab(ServerRequest req){
		String idTab = req.pathVariable("idTab");
		
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(tablero.get_Listas(idTab),Tablero.class);
	}
	
	public Mono<ServerResponse> get_DetalleTarjetas(ServerRequest req){
		String idTab = req.pathVariable("idTab");
		String idTarj = req.pathVariable("idTarj");
		
		return tablero.get_DetallesTarjeta(idTab, idTarj).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));				
	}
	
	public Mono<ServerResponse> get_AccionesTab(ServerRequest req){
		String idTab = req.pathVariable("idTab");
		
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(tablero.get_AccionesTab(idTab),Tablero.class);				
	}
	


//----------------------------------------------------------------------------------------------------------------------------------------	
	
	//Tarjeta
		
	public Mono<ServerResponse> get_Tajerta(ServerRequest req){
		String id = req.pathVariable("id");
		return tarjeta.findById(id).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));				
	}
	
	public Mono<ServerResponse> get_ListaTarjeta(ServerRequest req){
		String id = req.pathVariable("id");
		
		return tarjeta.get_ListaTarjeta(id).flatMap(t -> ServerResponse.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.bodyValue(t));
	}
	
	
//----------------------------------------------------------------------------------------------------------------------------------------	

	//Listas
	public Mono<ServerResponse> get_Lista(ServerRequest req){
		String id = req.pathVariable("id");
		return lista.findById(id).flatMap(l -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(l));				
	}
	
	public Mono<ServerResponse> get_TableroLista(ServerRequest req){
		String idLit = req.pathVariable("idList");
		return lista.get_TableroLista(idLit).flatMap(l-> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(l));
	}
	
	public Mono<ServerResponse> get_TarjetasLista(ServerRequest req){
		String id = req.pathVariable("id");
		
		return lista.get_TarjetasLista(id).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
	
//----------------------------------------------------------------------------------------------------------------------------------------	

	//Miembro
	
	public Mono<ServerResponse> get_Miembro(ServerRequest req){
		String id = req.pathVariable("id");
		return miembro.findById(id).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
				
	}
	
	public Mono<ServerResponse> get_AccionesMiembro(ServerRequest req){
		String id = req.pathVariable("id");
		return miembro.get_AccionesMiembro(id).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
	
//----------------------------------------------------------------------------------------------------------------------------------------	
	//Accion
	public Mono<ServerResponse> get_Accion(ServerRequest req){
		String id = req.pathVariable("id");
		return accion.findById(id).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
				
	}
	
	public Mono<ServerResponse> listarTableroAcc(ServerRequest req){
		String idAccion = req.pathVariable("idAccion");
		return accion.obtenerTablero(idAccion).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
	public Mono<ServerResponse> listarTarjetaAcc(ServerRequest req){
		String idAccion = req.pathVariable("idAccion");
		return accion.obtenerTarjeta(idAccion).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
	public Mono<ServerResponse> listarLitaAcc(ServerRequest req){
		String idAccion = req.pathVariable("idAccion");
		return accion.obtenerLista(idAccion).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
	public Mono<ServerResponse> listarMiembrosAcc(ServerRequest req){
		String idAccion = req.pathVariable("idAccion");
		return accion.obtenerMiembros(idAccion).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
	public Mono<ServerResponse> listarMiemCreadorAcc(ServerRequest req){
		String idAccion = req.pathVariable("idAccion");
		return accion.obtenerMiembroCreador(idAccion).flatMap(t -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.bodyValue(t));
	}
	
}
