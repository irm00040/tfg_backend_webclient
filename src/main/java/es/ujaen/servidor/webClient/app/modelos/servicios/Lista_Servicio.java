package es.ujaen.servidor.webClient.app.modelos.servicios;

import java.util.List;
import es.ujaen.servidor.webClient.app.modelos.Accion;

import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Tablero;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

public interface Lista_Servicio {
	
	
	public Mono<Lista> findById(String id);
	
	public Mono<Tablero> get_TableroLista(String idList);
	
	public Mono<List<Accion>> get_AccionesLista(String idList);
	
	public Mono<List<Tarjeta>> get_TarjetasLista(String id);
	
	

}
