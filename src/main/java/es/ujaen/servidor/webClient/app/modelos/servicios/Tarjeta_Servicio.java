package es.ujaen.servidor.webClient.app.modelos.servicios;

import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

public interface Tarjeta_Servicio {
		
	public Mono<Tarjeta> findById(String id);
	
	public Mono<Lista> get_ListaTarjeta(String id);

}
