package es.ujaen.servidor.webClient.app.modelos;

public class Miembro_Creador {
	
	private String id;
	private String fullName;
	private String initials;
	private boolean activityBlocked;
	private String avatarHash;
	private String avatarUrl;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public boolean isActivityBlocked() {
		return activityBlocked;
	}
	public void setActivityBlocked(boolean activityBlocked) {
		this.activityBlocked = activityBlocked;
	}
	public String getAvatarHash() {
		return avatarHash;
	}
	public void setAvatarHash(String avatarHash) {
		this.avatarHash = avatarHash;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	
	

}
