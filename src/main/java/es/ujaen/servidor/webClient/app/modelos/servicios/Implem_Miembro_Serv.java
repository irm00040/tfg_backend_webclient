package es.ujaen.servidor.webClient.app.modelos.servicios;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import es.ujaen.servidor.webClient.app.modelos.Accion;
import es.ujaen.servidor.webClient.app.modelos.Miembro;
import reactor.core.publisher.Mono;

@Service
public class Implem_Miembro_Serv implements Miembro_Servicio{

	@Autowired
	private WebClient client;
	
	@Override
	public Mono<Miembro> findById(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		return client.get()
				.uri("/members/{id}", params)
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Miembro.class);
	}


	@Override
	public Mono<List<Accion>> get_AccionesMiembro(String id) {
		Mono<List<Accion>> listaAcciones = client.get()
				.uri("/members/{id}/actions?limit=1000", Collections.singletonMap("id", id))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToFlux(Accion.class)
				.collectList();
		return listaAcciones;
	}
	

	
}