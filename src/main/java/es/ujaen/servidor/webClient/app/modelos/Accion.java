package es.ujaen.servidor.webClient.app.modelos;

import java.util.Date;

public class Accion {

	private String id;
	private String idMemberCreator;
	private String type;
	private Date date;	
	private Datos data;
	private Miembro_Creador memberCreator;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	
	public String getIdMemberCreator() {
		return idMemberCreator;
	}
	public void setIdMemberCreator(String idMemberCreator) {
		this.idMemberCreator = idMemberCreator;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Datos getData() {
		return data;
	}
	public void setData(Datos data) {
		this.data = data;
	}
	
	public Miembro_Creador getMemberCreator() {
		return memberCreator;
	}
	public void setMemberCreator(Miembro_Creador memberCreator) {
		this.memberCreator = memberCreator;
	}
}
