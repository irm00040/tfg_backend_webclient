package es.ujaen.servidor.webClient.app.modelos;

public class Tablero_Data {

	private String id;
	private String name;
	private String shortLink;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getShortLink() {
		return shortLink;
	}
	
	public void setShortLink(String shortLink) {
		this.shortLink = shortLink;
	}
}
