package es.ujaen.servidor.webClient.app.modelos;

import java.util.Date;
import java.util.List;

public class Tarjeta {

	private String id;
	private String name;
	private boolean closed;
	private String desc;
	private Date dateLastActivity;
	private String dueReminder;
	private String idBoard;
	private String idList;
	private List<String> idLabels;
	private List<Etiqueta> labels;
	private String shortUrl;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getDateLastActivity() {
		return dateLastActivity;
	}
	public void setDateLastActivity(Date dateLastActivity) {
		this.dateLastActivity = dateLastActivity;
	}
	public String getDueReminder() {
		return dueReminder;
	}
	public void setDueReminder(String dueReminder) {
		this.dueReminder = dueReminder;
	}
	public String getIdBoard() {
		return idBoard;
	}
	public void setIdBoard(String idBoard) {
		this.idBoard = idBoard;
	}
	public String getIdList() {
		return idList;
	}
	public void setIdList(String idList) {
		this.idList = idList;
	}
	
	public List<String> getIdLabels() {
		return idLabels;
	}
	public void setIdLabels(List<String> idLabels) {
		this.idLabels = idLabels;
	}
	
	public List<Etiqueta> getLabels() {
		return labels;
	}
	public void setLabels(List<Etiqueta> labels) {
		this.labels = labels;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

}
