package es.ujaen.servidor.webClient.app;




import java.io.IOException;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import es.ujaen.servidor.webClient.app.modelos.ReadJson;




@SpringBootApplication
public class SpringBootWebClientApplication {
	


	public static void main(String[] args) throws IOException {
		
		String url = "https://trello.com/b/vKq2Qi0w.json";
		
		SpringApplication.run(SpringBootWebClientApplication.class, args);
		ReadJson reader = new ReadJson();
		JSONObject json = reader.readJsonFromUrl(url);
		System.out.println(json.get("id"));

		
		
	}
	
}
