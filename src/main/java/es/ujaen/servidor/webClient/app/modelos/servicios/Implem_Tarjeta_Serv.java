package es.ujaen.servidor.webClient.app.modelos.servicios;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import es.ujaen.servidor.webClient.app.modelos.Lista;
import es.ujaen.servidor.webClient.app.modelos.Tarjeta;
import reactor.core.publisher.Mono;

@Service
public class Implem_Tarjeta_Serv implements Tarjeta_Servicio {
	
	@Autowired
	private WebClient client;

	@Override
	public Mono<Tarjeta> findById(String id) {
		return client.get()
				.uri("/cards/{id}",Collections.singletonMap("id", id))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Tarjeta.class);
	}

	public Mono<Lista> get_ListaTarjeta(String id) {
		Mono<Lista> columna = client.get()
				.uri("/cards/{id}/list", Collections.singletonMap("id", id))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Lista.class);
		return columna;
	}


}
